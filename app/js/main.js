let mobileMenu = document.querySelector('.open-menu');
let wrapper = document.querySelector('.wrapper');
let show = document.querySelector('.show-text');
let text = document.querySelector('.main-text__hide-block');
let faults = document.querySelector('.faults-more');
mobileMenu.addEventListener('click',function(){
	wrapper.classList.toggle('wrapper--open-menu');
});

if(show){
    show.addEventListener('click', function(){
        if (show.dataset['expand'] == 'true'){
            show.dataset['expand'] = false;
            show.innerText = 'Читать больше';
        } else{
            show.dataset['expand'] = true;
            show.innerText = 'Свернуть';
        }
        text.classList.toggle('main-text__hide-block--show');
    });
}
if (faults){
    faults.addEventListener('click', function(){
        document.querySelector('.faults').classList.add('faults--show');
        
        this.style.display = 'none';
    })
}
if (document.body.clientWidth < 768){
    let menu = document.getElementsByClassName('main-nav__item--parent'),
    links, link;
    for(var i = 0; i < menu.length; i++){
        link = menu[i].querySelector('.main-nav__link');
        link.addEventListener('click', function(event){
            event.preventDefault();
            this.parentElement.classList.toggle('open');
        })
    };
    const services = new Swiper('.services', {
        speed: 400,
        spaceBetween: 100,
        slidesPerView: 1,
        direction: 'horizontal',
        loop: true,
        pagination: {
            el: '.pagination__list',
        },
        navigation: {
            nextEl: '.pagination__btn--next',
            prevEl: '.pagination__btn--prev',
        }
    });
    const masters = new Swiper('.masters', {
        speed: 400,
        spaceBetween: 20,
        slidesPerView: 1,
        direction: 'horizontal',
        loop: true,
        centeredSlides: true,
        pagination: {
            el: '.pagination__list',
        },
        navigation: {
            nextEl: '.pagination__btn--next',
            prevEl: '.pagination__btn--prev',
        }
    });
    let priceBtn = document.getElementsByClassName('prices__btn');
    if (priceBtn){
        for(var i = 0; i < priceBtn.length; i++){
            priceBtn[i].addEventListener('click', function(){
                this.classList.toggle('open');
                this.parentElement.nextElementSibling.classList.toggle('open');
            })
        };
    }

}
const feedbacks = new Swiper('.feedbacks', {
    speed: 400,
    spaceBetween: 100,
    slidesPerView: 1,
    breakpoints: {
        768: {
          slidesPerView: 2,
          spaceBetween: 30
        },
    },
    slideClass: 'feedbacks__one',
    wrapperClass: 'feedbacks__list',
    pagination: {
        el: '.pagination__list',
    },
    navigation: {
        nextEl: '.pagination__btn--next',
        prevEl: '.pagination__btn--prev',
    }
});